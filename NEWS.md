# ihpdr 1.1.0

* `ihpd_browse`: new function that takes you to various webpages associated with international house price database.
* Bug Fix: Replaced numerical indexing to regexp for excel file selection.

# ihpdr 1.0.0.9000

* Added a `NEWS.md` file to track changes to the package.

# ihpdr 1.0.0

* CRAN Release
